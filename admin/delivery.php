<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mycloths";

//http://www.w3schools.com/php/php_mysql_update.asp

if(isset($_POST['select']))
{

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 

	$select=$_POST['select'];
	$status=$_POST['status'];	
	
	$sql = 'UPDATE track SET Status ="'.$status.'" WHERE ID = "'.$select.'"';
	$conn->query($sql);

	$conn->close();

}

?>

  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
	
	<main>
    <body>
		<div class="section" style="background-color:#00796b">
		  <div class="container">
				<div style="text-align:center">
				<a href="index.html" ><img src="../img/logo.png"></img></a>
				</div>
		  </div>
		</div>
		
		  <nav>
			<div class="nav-wrapper">
				<div class="container" style="text-align:center;">

				  <ul id="nav-mobile" class="hide-on-med-and-down">
					<li><a href="stock.html">Stock</a></li>
					<li class="active"><a  href="delivery.php">Delivery</a></li>
					<li ><a href="order.php">Order</a></li>
				  </ul>
				  
			   </div>	
			</div>
		  </nav>		
		  
		<br>
		<div class="container">
			<div class="row">
			  <div class="col s12">
				<div class="card-panel">
					<div class="row">
						<form class="col s12" method="post" action="delivery.php">
							 <div class="input-field col s12">
								<select name="select">
								  <option value="" disabled selected>Choose your option</option>
								  <?php 
									//http://www.w3schools.com/php/php_mysql_select.asp
									$conn = new mysqli($servername, $username, $password, $dbname);
									$sql2= "SELECT ID FROM track";
									$result = $conn->query($sql2);
									echo "success";
									if ($result->num_rows > 0) {
										// output data of each row
										while($row = $result->fetch_assoc()) {
											echo "<option value=\"". $row["ID"]."\">". $row["ID"]."</option>";
										}
									} else {
									echo "<option value= \"0\"> 0 results</option>";}
								  ?>

								</select>
								<label for="select">Choose your Track ID</label>
							</div>	
							
							<div class="input-field col s12">
								<select name="status">
								  <option value="Ordered" selected>Ordered</option>
								  <option value="Delivering" >Delivering</option>
								  <option value="Delivered" >Delivered</option>
								</select>
								<label for="status">Choose your Delivery Status</label>								
							</div>						
							
							<div class="right-align">
								<button class="waves-effect waves-light btn" type="submit" name="action">Submit</button>
							</div>
						</form>
					  </div>	

				</div>
			  </div>
			  
			</div>
		</div>
		 
		 
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>
	  <script type="text/javascript">
		  $(document).ready(function() {
			$('select').material_select();
		  });	  
	  
	  </script>
    </body>
	</main>
	
	<footer class="page-footer">
	  <div class="container">
		<div class="row">
		  <div class="col l6 s12">
			<h5 class="white-text">MyCloths.com.my</h5>
			<p class="grey-text text-lighten-4">Email: sales@mycloths.com.my</p>
		  </div>
		  <div class="col l4 offset-l2 s12">
			<h5 class="white-text">Links</h5>
			<ul>
			  <li><a class="grey-text text-lighten-3" href="#!">Facebook</a></li>
			  <li><a class="grey-text text-lighten-3" href="#!">Twitter</a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="footer-copyright">
		<div class="container">
		© 2016 MyCloths
		</div>
	  </div>
	</footer>
  </html>