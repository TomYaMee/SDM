<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
	
	<main>
    <body>
		<div class="section" style="background-color:#00796b">
		  <div class="container">
				<div style="text-align:center">
				<a href="index.html" ><img src="../img/logo.png"></img></a>
				</div>
		  </div>
		</div>
		
		  <nav>
			<div class="nav-wrapper">
				<div class="container" style="text-align:center;">

				  <ul id="nav-mobile" class="hide-on-med-and-down">
					<li><a href="stock.html">Stock</a></li>
					<li><a  href="delivery.php">Delivery</a></li>
					<li class="active"><a href="order.php">Order</a></li>
				  </ul>
				  
			   </div>	
			</div>
		  </nav>		
		  
		<br>
		<div class="container">
			<div class="row">
			  <div class="col s12">
				<div class="card-panel">	
				<!--http://www.w3schools.com/php/php_file_upload.asp -->
				Supplier, please upload your list of products here.
				 <form action="order.php" method="post" enctype="multipart/form-data">
					<label>Select file to upload:</label>
					<div class="file-field input-field">
					  <div class="btn">
						<span>File</span>
						<input type="file" name="fileupload" id="fileupload">
					  </div>
					  <div class="file-path-wrapper">
						<input class="file-path validate" type="text">
					  </div>
					</div>
	
					<?php
					//http://www.w3schools.com/php/php_file_upload.asp
					if(isset($_POST['action'])){

					$target_dir = "uploads/";
					$target_file = $target_dir . basename($_FILES["fileupload"]["name"]);
					$uploadOk = 1;
					$FileType = pathinfo($target_file,PATHINFO_EXTENSION);

					// Check if file already exists
					if (file_exists($target_file)) {
						echo "<span class=\"red-text\">Sorry, file already exists.</span>";
						$uploadOk = 0;
					}
					// Allow certain file formats
					else if($FileType != "pdf") {
						echo "<span class=\"red-text\">Sorry, only PDF files are allowed.</span>";
						$uploadOk = 0;
					}
					// Check if $uploadOk is set to 0 by an error
					else if ($uploadOk == 0) {
						echo "<span class=\"red-text\">Sorry, your file was not uploaded.</span>";
					// if everything is ok, try to upload file
					} else {
						if (move_uploaded_file($_FILES["fileupload"]["tmp_name"], $target_file)) {
							echo "<span class=\"green-text\">The file ". basename( $_FILES["fileupload"]["name"]). " has been uploaded.</span>";
						} else {
							echo "<span class=\"red-text\">Sorry, there was an error uploading your file.</span>";
						}
					}
					}
					?>	
	
					<div class="right-align">
						<button class="waves-effect waves-light btn" type="submit" name="action">Upload</button>
					</div>					
					
				
				  </form>
				</div>
			  </div>
			  
			</div>
		</div>
		
		<div class="container">
			<div class="row">
			  <div class="col s12">
				<div class="card-panel">	
					<div class="row">
					 <form action="order.php" method="post">
					  <div class="col s12">
					  Staffs please send your order here.
					  <br>
					  A list of products sent by suppliers can be found <a href="uploads/" target="_blank"> here </a>
					  </div>
					  <div class="input-field col s12">
						
						<select name="supplier">
						  <option value="" disabled selected>Choose your option</option>
						  <option value="1">Supplier A</option>
						  <option value="2">Supplier B</option>
						  <option value="3">Supplier C</option>
						</select>
						<label for="supplier">Select your supplier</label>
					  </div>	

						<div class="input-field col s12">
						  <textarea name="message" id="message" class="materialize-textarea"></textarea>
						  <label for="message">Message</label>
						</div>			

						 <?php 
						 //if "email" variable is filled out, send email
						  if (isset($_POST['supplier']))  {
						  
						  //Email information
						  $admin_email = "tommy9611@gmail.com";
						  $comment = $_POST['message'];
						  
						  //send email
						  mail($admin_email, "Order List", $comment, "From: MyCloths" );
						  
						  //Email response
						  echo "<div class=\"col s12\"><span class=\"green-text\">Order sent.</span></div>";
						  }
						 ?>						
						
						<div class="right-align">
							<button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
						</div>						
						
					</form>
				  </div>
				</div>
			  </div>
			</div>
		</div>
		 
		 
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>
	  <script type="text/javascript">
		  $(document).ready(function() {
			$('select').material_select();
		  });	  
	  
	  </script>
    </body>
    </body>
	</main>
	
	<footer class="page-footer">
	  <div class="container">
		<div class="row">
		  <div class="col l6 s12">
			<h5 class="white-text">MyCloths.com.my</h5>
			<p class="grey-text text-lighten-4">Email: sales@mycloths.com.my</p>
		  </div>
		  <div class="col l4 offset-l2 s12">
			<h5 class="white-text">Links</h5>
			<ul>
			  <li><a class="grey-text text-lighten-3" href="#!">Facebook</a></li>
			  <li><a class="grey-text text-lighten-3" href="#!">Twitter</a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="footer-copyright">
		<div class="container">
		© 2016 MyCloths
		</div>
	  </div>
	</footer>
  </html>