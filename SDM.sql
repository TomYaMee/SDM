CREATE DATABASE mycloths;

USE mycloths;
CREATE TABLE track (ID int, Status varchar(15));

INSERT INTO track (ID, Status) VALUES (12345, "Delivering");
INSERT INTO track (ID, Status) VALUES (23456, "Ordered");
INSERT INTO track (ID, Status) VALUES (34567, "Delivered");